﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolsSetup : MonoBehaviour
{

    [SerializeField]
    private PoolManager.PoolPart[] pools;

    private void Awake()
    {
        PoolManager.Initialization(pools, gameObject);
    }

 
}
