﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PoolManager 
{

    private static PoolPart[] pools;

    [System.Serializable]
    public struct PoolPart
    {
        public string name;
        public GameObject prefab;
        public int count;
        [System.NonSerialized]
        public ObjectPooling objectPooling;
    }

  
    public static GameObject GetObject()
    {
        
        return null;
    }

    public static void Initialization(PoolPart[] inputPools, GameObject objectsParent)
    {
        pools = inputPools;

        for (int i = 0; i < pools.Length; i++)
        {
            if (pools[i].prefab != null)
            {
                GameObject poolObject = new GameObject();
                poolObject.name = "Pool" + pools[i].name;
                poolObject.transform.SetParent(objectsParent.transform);
                poolObject.AddComponent<ObjectPooling>();
                pools[i].objectPooling = poolObject.GetComponent<ObjectPooling>();
                pools[i].objectPooling.GetComponent<ObjectPooling>().Initialization(pools[i].prefab, pools[i].count);
            }
        }
    }

    public static GameObject GetObject(string name, Vector3 position, Vector3 rotation)
    {
        GameObject result = null;

        if(pools != null)
        {
            foreach (var item in pools)
            {
                if(string.Compare(item.name, name) == 0)
                {
                    result = item.objectPooling.GetObject();
                    result.transform.position = position;
                    result.transform.eulerAngles = rotation;
                    result.SetActive(true);
                    return result;
                }
            }
        }

        return result;

    }
}
