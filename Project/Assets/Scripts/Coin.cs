﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform followTo;
    private bool isActive;
    private Tween tween;
    private Vector3 jump;
    void Awake()
    {
        tween = DOTween.Sequence();
        followTo = null;
        isActive = true;
    }

    private void Update()
    {
        if (followTo != null) {
            Vector3 follow = new Vector3(transform.position.x, transform.position.y, followTo.position.z);
            transform.position = follow;
        }
    }

    private void Repeat()
    {
        if (isActive) tween = transform.DOJump(jump, 1, 3, 1f, false).SetEase(Ease.Linear).SetDelay(3).OnComplete(Repeat);
    }

    public void StartJumping(Vector3 pos)
    {
        jump = pos;
        isActive = true;
        
        Repeat();
    }

    private void OnDisable()
    {
        tween.Kill();
        isActive = false;
        followTo = null;

    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wood") followTo = collision.transform;
    }

}
