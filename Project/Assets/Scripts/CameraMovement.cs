﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraMovement : MonoBehaviour
{
    public GameObject player;
    private Vector3 difference;
    void Start()
    {
        difference = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Game.IsGameOver) return;
        if(transform.position - player.transform.position != difference)
        {
            Vector3 position = difference + player.transform.position;
            position.y = transform.position.y;
            transform.DOMove(position, 1.5f, false);
        }
    }
}
