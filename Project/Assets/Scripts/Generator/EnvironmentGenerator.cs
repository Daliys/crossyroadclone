﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnvironmentGenerator : MonoBehaviour
{
    protected List<GameObject> environmentList;
    public GameObject coinPrefab;

    public abstract void GenerateEnvironment() ;
    public virtual void ReturnToPool()
    {
        foreach (var item in environmentList)
        {
            item.GetComponent<PoolObject>().ReturnToPool();
        }
        environmentList.Clear();
        transform.GetComponent<PoolObject>().ReturnToPool();
    }

    protected virtual void GenerateCoin()
    {
        int randPosition = (int)Random.Range(-Game.movementAvalible.z, Game.movementAvalible.z);
        foreach (var item in environmentList)
        {
            if (item.transform.position.z == randPosition)
            {
                GenerateCoin();
                return;
            }
        }
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1, randPosition);
        GameObject gm = PoolManager.GetObject(coinPrefab.name, pos, Vector3.zero);
        gm.GetComponent<Coin>().StartJumping(pos);
        environmentList.Add(gm);
    }

    protected virtual void Awake()
    {
        environmentList = new List<GameObject>();
    }

}


