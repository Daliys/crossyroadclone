﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : EnvironmentGenerator
{

    public GameObject[] cars;
    public float carsSize;

    private float carDistance = 1.5f;

    public override void GenerateEnvironment()
    {
        bool isDirectionLeft = Random.value > 0.5f;
        float range = Game.groundSizeZ / 2;
        float vehicleSpeed = Random.Range(3,7);
        float delay = Random.Range(0, 2);

        for (int i = 0; i < Random.Range(1,4); i++)
        {
            Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1, range);
            GameObject gm = PoolManager.GetObject(cars[Random.Range(0, cars.Length)].name, pos, Vector3.zero);
            gm.GetComponent<TransportMovement>().StartMovement(range, -range, GetRandomPosition(range) ,isDirectionLeft, vehicleSpeed, delay);

            environmentList.Add(gm);
        }

        if (Random.value < Game.dropProbabilityOfCoin) GenerateCoin();
    }

    public float GetRandomPosition(float range)
    {
        float random = Random.Range(-range, range);
        float sumDistance = carsSize / 2 + carDistance; 
        foreach (var item in environmentList)
        {
            if (random < item.transform.position.z - sumDistance || random > item.transform.position.z + sumDistance) continue;
            else return GetRandomPosition(range);
        }

        return random;
    }


}
